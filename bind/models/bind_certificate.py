# - - coding: utf-8 - -
from odoo import models, fields, api
class BindCertificate (models.Model) :
    _name = 'bind.certificate'
    _rec_name = 'type'
    _description = 'Certificado BIND'
    username = fields.Char ('Usuario', required=True)
    password = fields.Char ('Contraseña', required= True)
    passphrase = fields.Text ('Passphrase')
    type = fields.Selection(
        [('production', 'Produccion'),
         ('homologation', 'Homologacion'),
         ('sandbox','SandBox')],
        'Type',
        required=True,
    )
