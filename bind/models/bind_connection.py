# - - coding: utf-8 - -
from odoo import models, fields, api
import requests, datetime
import json
import pycurl
try:
    from StringIO import StringIO as BytesIO
except ImportError:
    from io import BytesIO
import base64
from tempfile import TemporaryFile

sess = requests.session()
import logging
_logger = logging.getLogger(__name__)


class BindConnection (models.Model) :
    _name = 'bind.connection'
    _description = 'Conexion BIND'
    certificate = fields.Many2one(
    	'bind.certificate',
    	'Certificate',
 		required=True
    )
    token = fields.Text(
        'Token',
        readonly=True
    )
    generationtime = fields.Datetime(
        'Generation Time',
        readonly=True
    )
    expirationtime = fields.Datetime(
        'Expiration Time',
        readonly=True
    )
    expire_in = fields.Text(
        'Expire in',
        readonly=True
    )

    @api.multi
    def validate_token(self):
        # cantidad_certificados= self.search([('certificate', '=',self.certificate.type)])
        # if  len(cantidad_certificados)<1 or self.expirationtime<datetime.datetime.now():
        if  self.expirationtime is False or self.expirationtime < datetime.datetime.now():
            self.connect()
        return True

    @api.multi
    def connect(self):
        USER = self.certificate.username
        PASS = self.certificate.password
        host = 'api-qa.bind.com.ar'
        data = {"username": USER, "password": PASS}
        header = ['Accept: application/json', "Content-type: application/json"]
        b = BytesIO()
        c = pycurl.Curl()
        if self.certificate.type == "sandbox":
            # data = {"username": self.certificate.username, "password": self.certificate.password}
            # res = sess.post('https://sandbox.bind.com.ar/v1/login/jwt', json=data)
            request_url = 'https://sandbox.bind.com.ar/v1/login/jwt'
        elif self.certificate.type == "homologation":
            # USER = self.certificate.username
            # PASS = self.certificate.password
            passphrase = self.certificate.passphrase
            #certificate_file = base64.encodestring(self.certificate.certificate_file.encode('utf-8'))
           
            # certificate_file = self.certificate.sslcert_file
            #certificate_secret = base64.b64decode(self.certificate.certificate_key)
            #certificate_secret = base64.encodestring(self.certificate.certificate_key.encode('utf-8'))
           

            # certificate_secret = self.certificate.sslkey_file
            # host = 'api-qa.bind.com.ar'
            request_url = 'https://api-qa.bind.com.ar/v1/login/jwt'
            # data = {"username": USER, "password": PASS}
            # header = ['Accept: application/json', "Content-type: application/json"]
            # b = BytesIO()
            # c = pycurl.Curl()
            # c.setopt(pycurl.URL, request_url)
            # c.setopt(pycurl.WRITEDATA, b)
            # c.setopt(pycurl.URL, request_url)
            # c.setopt(pycurl.WRITEDATA, b)
            c.setopt(pycurl.SSLKEY, '/key_file')
            c.setopt(pycurl.SSLCERT, '/cert_file')
            c.setopt(pycurl.SSLKEYPASSWD, passphrase)
            # c.setopt(pycurl.HTTPHEADER, header)
            # c.setopt(pycurl.POSTFIELDS,json.dumps(data))
            #c.setopt(pycurl.VERBOSE, True)
            # c.perform()
            # c.close()
            # res = b.getvalue()
        elif self.certificate.type == "produccion":
            res = sess.post('https://sandbox.bind.com.ar/v1/login/jwt', json=data)
        c.setopt(pycurl.URL, request_url)
        c.setopt(pycurl.WRITEDATA, b)
        c.setopt(pycurl.HTTPHEADER, header)
        c.setopt(pycurl.POSTFIELDS,json.dumps(data))
        c.perform()
        c.close()
        # res = b.getvalue()
        # res = res.json()
        response_body = b.getvalue()
        res = json.loads(response_body.decode('utf-8'))
        self.write({'token':res['token'],
                    'generationtime': self.write_date,
                    'expirationtime': self.write_date+datetime.timedelta(
                        seconds=res['expires_in']),
                    'expire_in': res['expires_in']})
        return True
