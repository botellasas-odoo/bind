{
	'name' : 'BIND',
	'description': 'Conexion y Certificado BIND',
	'author': 'Matias',
	'depends': ['base'],
	'application' : True,
	'data' : ['views/bind_menu.xml',
	'views/bind_certificate.xml',
	'views/bind_connection.xml',
	'security/ir.model.access.csv']
}