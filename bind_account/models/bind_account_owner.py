# - - coding: utf-8 - -
from odoo import models, fields


class BindAccountOwner(models.Model):
    _name = 'bind.account.owner'
    _description = 'Propietario Cuenta BIND'

    owner_id = fields.Text(
        "Id",
        readonly=True
    )
    display_name = fields.Text(
        "Nombre",
        readonly=True
    )
    id_type = fields.Text(
        "Tipo",
        readonly=True
        )
    is_physical_person = fields.Boolean(
        "Persona Fisica?",
        readonly=True
    )

