# - - coding: utf-8 - -
from odoo import models, fields, api
import requests
from odoo.exceptions import UserError


class BindAccount(models.Model):
    _name = 'bind.account'
    _rec_name = "account_id"
    _description = 'Cuenta BIND'
    account_id = fields.Text(
        'Id',
        readonly=True
    )
    label = fields.Text(
        'Alias',
        readonly=True
    )
    number = fields.Text(
        'Numero de Cuenta',
        readonly=True
    )
    type = fields.Text(
        'Tipo',
        readonly=True
    )
    status = fields.Text(
        'Estado',
        readonly=True
    )
    owners = fields.Many2many(
        "bind.account.owner",
        "owners_account_rel",
        "account_id",
        "owner_id",
        "Propietarios",
        auto_join=True,
        readonly=True
    )
    balance_currency = fields.Selection(
        [('1', 'ARS'),
         ('2', 'USD'),
         ('80', 'VARIOS'),
         ('ARS', 'ARS'),
         ('USD', 'USD')],
        "Moneda Saldo",
        readonly=True
    )
    balance_amount = fields.Float(
        "Monto Saldo",
        (8, 2),
        readonly=True
        )
    bank_id = fields.Text(
        "Codigo Banco",
        readonly=True
    )
    account_routing_scheme = fields.Text(
        "Tipo Cuenta",
        readonly=True
    )

    account_routing_address = fields.Text(
        "Valor Cuenta",
        readonly=True
    )
    transactions = fields.One2many(
        'bind.transaction',
        'from_account',
        'Transacciones'
    )