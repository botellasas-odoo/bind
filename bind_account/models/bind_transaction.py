# - - coding: utf-8 - -
from odoo import models, fields, api
import requests
from odoo.exceptions import UserError


class BindTransaction(models.Model):
    _name = 'bind.transaction'
    _description = 'Trasaccion BIND'

    transaction_id = fields.Text(
        'Id',
        readonly=True
    )
    # type string
    type = fields.Text(
        'Tipo',
        readonly=True
    )
    # from_bank_id string
    from_bank_id = fields.Text(
        'Banco Cuenta Origen',
        readonly=True
    )
    # from_account_id string
    from_account = fields.Many2one(
        'bind.account',
        'Cuenta Origen',
        readonly = True
    )
    
    # counterparty_id string
    counterparty_id = fields.Text(
        'ID Cuenta Tercero',
        readonly=True
    )
    # counterparty_id_type string
    counterparty_id_type = fields.Text(
        'Tipo ID Cuenta Tercero',
        readonly=True
    )
    # counterparty_name string
    counterparty_name = fields.Text(
        'Nombre de Tercero',
        readonly=True
    )
    # counterparty_bank_routing_scheme string
    counterparty_bank_routing_scheme = fields.Text(
        'Tipo Banco de Tercero',
        readonly=True
    )
    # counterparty_bank_routing_address string
    counterparty_bank_routing_address = fields.Text(
        'Nombre Banco de Tercero',
        readonly=True
    )
    # counterparty_bank_routing_code string
    counterparty_bank_routing_code = fields.Text(
        'Codigo Banco de Tercero',
        readonly=True
    )
    # account_routing_scheme string
    account_routing_scheme = fields.Text(
        'CBU/Alias',
        readonly=True
    )
    # account_routing_address string
    account_routing_address = fields.Text(
        'Valor CBU/Alias',
        readonly=True
    )
    # details_type string
    details_type = fields.Text(
        'Origen de Tranferencia',
        readonly=True
    )
    # details_origin_id string
    details_origin_id = fields.Text(
        'ID Origen',
        readonly=True
    )
    # transaction_ids []
    transaction_ids = fields.Text(
        'ID Transacciones',
        readonly=True
    )
    # status string
    status = fields.Text(
        'Estado',
        readonly=True
    )

    start_date = fields.Datetime(
        'Fecha de Inicio',
        readonly=True
    )
    end_date = fields.Datetime(
        'Fecha de Fin',
        readonly=True
    )
    challenge = fields.Text(
        'Challenge',
        readonly=True
    )
    charge_summary = fields.Text(
        'Descripcion Cargos',
        readonly=True
    )
    charge_value_currency = fields.Text(
        'Moneda',
        readonly=True
    )
    charge_value_amount = fields.Float(
        "Monto",
        (8, 2),
        readonly=True
        )