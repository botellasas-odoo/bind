{
    'name' : 'BIND ACCOUNT',
    'description': 'Cuentas BIND',
    'author': 'Matias',
    'depends': ['bind'],
    'application' : True,
    'data' : ['views/bind_account_menu.xml',
              'views/bind_account.xml',
              'views/bind_transaction.xml',
              'views/bind_account_owner.xml',
              'wizards/bind_account_wizard.xml',
              'security/ir.model.access.csv']
}