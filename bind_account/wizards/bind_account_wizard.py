# - - coding: utf-8 - -
from odoo import models, fields, api
import requests
from odoo.exceptions import UserError
import json
import pycurl
try:
    from StringIO import StringIO as BytesIO
except ImportError:
    from io import BytesIO
import base64
from tempfile import TemporaryFile

sess = requests.session()
import logging
_logger = logging.getLogger(__name__)



class BindAccountWizard(models.TransientModel):
    _name = 'bind.account.wizard'
    _description = 'Cuenta BIND Wizard'
    
    @api.multi
    def actualizar_datos(self):
        bind_mode = self.env['ir.config_parameter'].sudo().get_param('bind_mode')
        connection = self.env['bind.connection'].search([('certificate', '=', bind_mode)])
        connection.validate_token()
        TOKEN = connection.token
        base_url = self.get_url(connection.certificate.type)
        header = ['Accept: application/json',
        'Content-type: application/json',
        'Authorization:JWT '+TOKEN] 
        self.consultar_cuenta(TOKEN, header, connection, base_url)
        self.consultar_transferencias(TOKEN, header, connection, base_url, "TRANSFERENCIAS_ENVIADAS")
        self.consultar_transferencias(TOKEN, header, connection, base_url, "TRANSFERENCIAS_RECIBIDAS")
        return True
        # return {
        #     'type': 'ir.actions.client',
        #     'tag': 'reload',
        #     }  

    @api.multi
    def get_url(self, prod_environment):
        if prod_environment == "sandbox":
            base_url = 'https://sandbox.bind.com.ar/v1/banks/322/accounts'
        elif prod_environment == "homologation":
            base_url = 'https://api-qa.bind.com.ar/v1/banks/322/accounts'
        elif prod_environment == "production":
            base_url = 'https://sandbox.bind.com.ar/v1/banks/322/accounts'    
        return base_url
    
    @api.multi
    def consultar_cuenta(self, TOKEN, header, connection, base_url):
        # bind_mode = self.env['ir.config_parameter'].sudo().get_param('bind_mode')
        # connection = self.env['bind.connection'].search([('certificate', '=', bind_mode)])
        # connection.validate_token()
        # TOKEN = connection.token
        # base_url = self.get_url(connection.certificate.type)
        # header = ['Accept: application/json',
        # 'Content-type: application/json',
        # 'Authorization:JWT '+TOKEN] 
        request_url = '%s/owner'%(base_url)
        accounts = self.set_opt_pycurl(header, request_url, connection)
        for res in accounts:           
            owners_ids = []
            account_id = res['id']
            label = res['label']
            number = res['number']
            type = res['type']
            status = res['status']
            balance_currency = res['balance']['currency']
            balance_amount = res['balance']['amount']
            bank_id = res['bank_id']
            account_routing_scheme = res['account_routing']['scheme']
            account_routing_address = res['account_routing']['address']
            datos_cuenta = {'account_id': account_id, 'label':label, 'number': number, 'type':type, 'status':status, 'balance_currency':balance_currency,
                               'balance_amount':balance_amount, 'bank_id':bank_id, 'account_routing_scheme':account_routing_scheme, 'account_routing_address':account_routing_address}
            account = self.env['bind.account'].search([('account_id', '=', res['id'])])
            if account:
                account.write(datos_cuenta)
            else:
                account = self.env['bind.account'].create(datos_cuenta)
            for owner in res['owners']:
                owner['owner_id'] = owner['id']
                search_owner = self.env['bind.account.owner'].search([('owner_id', '=', owner['owner_id'])])
                if search_owner:
                    search_owner.write(owner)
                else:
                    search_owner = self.env['bind.account.owner'].create(owner)
                owners_ids.append(search_owner.id)
            account.owners = owners_ids
            # self.consultar_transferencias(TOKEN, header, account, connection, base_url, "TRANSFERENCIAS_ENVIADAS")
            # self.consultar_transferencias(TOKEN, header, account, connection, base_url, "TRANSFERENCIAS_RECIBIDAS")
        return accounts

    def consultar_transferencias(self, TOKEN, header, connection, base_url, type_origin):
        #header.append('obp_origin: %s'%(type_origin))
        accounts = self.env['bind.account'].search([])
        if  type_origin == "TRANSFERENCIAS_ENVIADAS":
            header = ['Accept: application/json',
            'Content-type: application/json',
            'Authorization:JWT '+TOKEN] 
        elif type_origin == "TRANSFERENCIAS_RECIBIDAS":
            header = ['Accept: application/json',
            'Content-type: application/json',
            'Authorization:JWT '+TOKEN,
            'obp_origin: TRANSFERENCIAS_RECIBIDAS'] 
        for account in accounts:
            request_url = '{}/{}/owner/transaction-request-types/TRANSFER'.format(base_url, account.account_id)
            res = self.set_opt_pycurl(header,request_url,connection)
            counterparty_id = None
            counterparty_name = None
            counterparty_bank_routing_code = None
            challenge = None
            details_type = None
            details_origin_id = None
            for transaction in res:
                if 'id' in transaction['counterparty']:
                    counterparty_id = transaction['counterparty']['id']
                if 'name' in transaction['counterparty']:
                    counterparty_name = transaction['counterparty']['name']
                if 'code' in transaction['counterparty']['bank_routing']:
                    counterparty_bank_routing_code = transaction['counterparty']['bank_routing']['code']
                if 'challenge' in transaction:
                    challenge = transaction['challenge']
                if 'details' in transaction:
                    if 'type' in transaction['details']:
                        details_type = transaction['details']['type']
                    if 'origin_id' in transaction['details']:
                        details_origin_id = transaction['details']['origin_id']
                    else:
                        details_origin_id = None
                transaction_start_date = transaction['start_date'].split('T')
                transaction_start_date = transaction_start_date[0]+" "+transaction_start_date[1]
                transaction_end_date = transaction['end_date'].split('T')
                transaction_end_date = transaction_end_date[0]+" "+transaction_end_date[1]
                datos_transferencia = {'transaction_id':transaction['id'],
                                    'type':transaction['type'],
                                    'from_bank_id':transaction['from']['bank_id'],
                                    'counterparty_id': counterparty_id,
                                    'counterparty_id_type':transaction['counterparty']['id_type'],
                                    'counterparty_name':counterparty_name,
                                    'counterparty_bank_routing_scheme':transaction['counterparty']['bank_routing']['scheme'],
                                    'counterparty_bank_routing_address':transaction['counterparty']['bank_routing']['address'],
                                    'counterparty_bank_routing_code':counterparty_bank_routing_code,
                                    'account_routing_scheme':transaction['counterparty']['account_routing']['scheme'],
                                    'account_routing_address':transaction['counterparty']['account_routing']['address'],
                                    'details_type':details_type,
                                    'details_origin_id':details_origin_id,
                                    'status':transaction['status'],
                                    'start_date':transaction_start_date,
                                    'end_date':transaction_end_date,
                                    'challenge':challenge,
                                    'charge_summary':transaction['charge']['summary'],
                                    'charge_value_currency':transaction['charge']['value']['currency'],
                                    'charge_value_amount':transaction['charge']['value']['amount']}
                transaction = self.env['bind.transaction'].search([('transaction_id', '=', transaction['id'])])
                if transaction:
                    transaction.write(datos_transferencia)
                else:
                    transaction = self.env['bind.transaction'].create(datos_transferencia)
                transaction.from_account = account.id
        return True

    def realizar_transferencia(self, TOKEN, header,account, connection, base_url):
        # bind_mode = self.env['ir.config_parameter'].sudo().get_param('bind_mode')
        # connection = self.env['bind.connection'].search([('certificate', '=', bind_mode)])
        # connection.validate_token()
        TOKEN = connection.token
        base_url = self.get_url(connection.certificate.type)
        request_url = '{}/{}/owner/transaction-request-types/TRANSFER/transaction-requests'.format(base_url, account.account_id)
        header = ['Accept: application/json',
        'Content-type: application/json',
        'Authorization:JWT '+TOKEN] 
        data = {
            "to": {
                "label": "FACUPRUEBADEBIN"
            },
            "value": {
                "currency": "ARS",
                "amount": 106.00
            },
            "description": "Transferencia Enviada",
            "concept": "VAR",
            "emails": [
                "apibank@poincenot.com"
            ]
        }
        self.set_opt_pycurl(header, request_url, connection, data)
        self.consultar_transferencias(TOKEN, header, connection, base_url, "TRANSFERENCIAS_ENVIADAS")
        self.consultar_transferencias(TOKEN, header, connection, base_url, "TRANSFERENCIAS_RECIBIDAS")
        return True
    
    def set_opt_pycurl(self, header,request_url, connection, data = {} ):
        b = BytesIO()
        c = pycurl.Curl()
        if connection.certificate.type == "homologation":
            passphrase = connection.certificate.passphrase
            c.setopt(pycurl.SSLKEY, '/key_file')
            c.setopt(pycurl.SSLCERT, '/cert_file')
            c.setopt(pycurl.SSLKEYPASSWD, passphrase)
        c.setopt(pycurl.URL, request_url)
        c.setopt(pycurl.WRITEDATA, b)
        c.setopt(pycurl.HTTPHEADER, header)
        if data != {}:
            c.setopt(pycurl.POSTFIELDS,json.dumps(data))
        c.perform()
        c.close()
        response_body = b.getvalue()
        res = json.loads(response_body.decode('utf-8'))
        return res
