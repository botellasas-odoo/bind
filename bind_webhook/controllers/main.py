import logging
import pprint
import werkzeug
import pprint
import requests

from odoo import http
from odoo.http import request, Response

_logger = logging.getLogger(__name__)

class BindController(http.Controller):

    def create_ticket(self, name, data):
        User = request.env['res.users']
        Ticket = request.env['helpdesk.ticket']
        team = request.env['helpdesk.team'].sudo().search([
            ('alias_name', '=', 'asistenciabind')
        ])
        stage = request.env['helpdesk.stage'].sudo().search([
            ('sequence', '=', 0)
        ])
        _logger.info('team %s', team)
        user = User.sudo().search([
            ('login', '=', 'santiago'),
        ])
        vals = {
            'name': name,
            'description': data,
            'user_id': user and user.id or False,
            'team_id': team and team.id or False,
            'stage_id': stage and stage.id or False,
        }
        _logger.info('vals %s', vals)
        Ticket.sudo().create(vals)
        _logger.info('creando ticket')
        return True

    @http.route(['/webhook'], type='json', auth="public", csrf=False)
    def bind_webhook(self, **kw):
        Sale = request.env['sale.order']
        Mail = request.env['mail.mail']
        _logger.info('Recibiendo webhook de Bind ------------------------------- ')
        _logger.info('parametro: %s', str(kw))
        data = request.jsonrequest
        _logger.info('\n%s',  pprint.pformat(data))
        # buscar sale.order que coincida en estado pendiente , que coincida por amount
        amount = data['data']['charge']['value']['amount']
        _logger.info('amount %s', amount)
        #return {
        #    "status": "OK"
        #}
        sale = Sale.sudo().search([
            ('state', '=', 'sent'),
            ('amount_total', '=', amount),
        ])
        _logger.info('sale %s', sale)
        if not sale:
            asunto = 'WEBHOOK: No se encontraron pedidos pendientes por el monto %s'% amount
            _logger.info(asunto)
            self.create_ticket(
                asunto,
                pprint.pformat(data)
            )
        elif len(sale) == 1:
            _logger.info('Se encontró una pedido pendiente ID %s - monto %s', sale.id, amount)
            sale.action_confirm()
            _logger.info('Pedido matcheado con exito!')
            # TODO: enviar mail informando que se matcheo
        else:
            pedidos = ''
            for s in sale:
                pedidos += '%s / '%s.name
            asunto = 'WEBHOOK: Se encontró mas de un pedido %s - monto %s'%(pedidos, amount)
            _logger.info(asunto)
            self.create_ticket(
                asunto,
                pprint.pformat(data)
            )
        return Response("TEST", content_type='text/html;charset=utf-8', status=200)


# {'created': '2018-09-12T22:13:07.169Z',
#  'data': {'challenge': None,
#           'charge': {'summary': '', 'value': {'amount': 10, 'currency': 'ARS'}},
#           'counterparty': {'account_routing': {'address': 'UNAVAILABLE',
#                                                'scheme': 'UNAVAILABLE'},
#                            'bank_routing': {'address': None,
#                                             'scheme': 'UNAVAILABLE'},
#                            'id': 'UNAVAILABLE',
#                            'id_type': 'UNAVAILABLE',
#                            'name': 'UNAVAILABLE'},
#           'details': {'origin_credit': {'cuit': '30615423323',
#                                         'cvu': '0000031400000000000031'}},
#           'end_date': '2018-04-21T00:15:01.764-03:00',
#           'from': {'account_id': '21-1-99999-4-6', 'bank_id': '322'},
#           'id': 'ESTE1ES2UN3ID4DE5DEBIN',
#           'start_date': '2018-04-21T00:15:01.764-03:00',
#           'status': 'COMPLETED',
#           'transaction_ids': ['ESTE1ES2UN3ID4DE5DEBIN'],
#           'type': 'TRANSFER'},
#  'id': '9ff3a818-91a1-464c-b368-103fc88fd7a5',
#  'object': 'ApiTransaction',
#  'redeliveries': 0,
#  'type': 'transfer.cvu.received'}
