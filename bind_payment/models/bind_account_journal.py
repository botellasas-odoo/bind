# -*- coding: utf-8 -*-
from odoo import models, fields, api

class BindAccountJournal(models.Model):
    _inherit = "account.journal"

    is_bind = fields.Boolean(
        "Es Banco Industrial"
    )