# -*- coding: utf-8 -*-
from odoo import models, fields, api
import json
import pycurl
try:
    from StringIO import StringIO as BytesIO
except ImportError:
    from io import BytesIO
import base64
import logging
_logger = logging.getLogger(__name__)

class BindPayment(models.Model):
    _inherit ='account.payment.group'


    @api.multi
    def post(self):
        res = super(BindPayment, self).post()
        print ('metodo post heredado')
        bind_mode = self.env['ir.config_parameter'].sudo().get_param('bind_mode')
        connection = self.env['bind.connection'].search([('certificate', '=', bind_mode)])
        connection.validate_token()
        TOKEN = connection.token
        # base_url = self.get_url(connection.certificate.type)
        # request_url = '{}/{}/owner/transaction-request-types/TRANSFER/transaction-requests'.format(base_url, account.account_id)
        request_url = 'https://api-qa.bind.com.ar/v1/banks/322/accounts/20-1-629891-1-5/owner/transaction-request-types/TRANSFER/transaction-requests'
        header = ['Accept: application/json',
        'Content-type: application/json',
        'Authorization:JWT '+TOKEN] 
        for rec in self:
            ## comprobaciones:
            # si es pago de proveedor (existe los pagos de cliente)
            # si metodo de pago es banco industrial
                # en el diario banco industrial agregar tilde: trasnferencia_bind
            # payment_type_copy --> enviar dinero outbound
            ## ----> armar el diccionar de datos y enviar al end point de transferencia de bind
            # https://sandbox.bind.com.ar/apidoc/#api-Transferencia-CrearTransferencia
            for payment in rec.payment_ids:
                if payment.journal_id.is_bind == True:
                    origin_id = payment.document_sequence_id.prefix+str(payment.document_sequence_id.number_next_actual-1).zfill(8)
                    origin_split=origin_id.split(sep="-")
                    origin_id= origin_split[0]+origin_split[1]
                    _logger.info(origin_id)
                    if rec.communication==False:
                        description = "Transferencia Enviada"
                    else:
                        description = rec.communication
                    data = {
                        "origin_id": origin_id,
                        # "origin_id": "3333333",
                        "to": {
                            "cbu": rec.matched_move_line_ids.invoice_id.partner_bank_id.acc_number
                        },
                        "value": {
                            "currency": payment.currency_id.name,
                            "amount": payment.amount
                        },
                        "description": description,
                        "concept": rec.matched_move_line_ids.invoice_id.bind_concept,
                        "emails": [
                            rec.partner_id.email
                        ]
                    }
                    self.set_opt_pycurl(header, request_url, connection, data)
            print ('entre al for!')
        self.env['bind.account.wizard'].actualizar_datos()
        return res

    def set_opt_pycurl(self, header,request_url, connection, data = {} ):
        b = BytesIO()
        c = pycurl.Curl()
        if connection.certificate.type == "homologation":
            passphrase = connection.certificate.passphrase
            c.setopt(pycurl.SSLKEY, '/key_file')
            c.setopt(pycurl.SSLCERT, '/cert_file')
            c.setopt(pycurl.SSLKEYPASSWD, passphrase)
        c.setopt(pycurl.URL, request_url)
        c.setopt(pycurl.WRITEDATA, b)
        c.setopt(pycurl.HTTPHEADER, header)
        if data != {}:
            c.setopt(pycurl.POSTFIELDS,json.dumps(data))
        c.perform()
        c.close()
        response_body = b.getvalue()
        res = json.loads(response_body.decode('utf-8'))
        return res
