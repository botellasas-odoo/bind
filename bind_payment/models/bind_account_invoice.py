# -*- coding: utf-8 -*-
from odoo import models, fields, api

class BindAccountInvoice(models.Model):
    _inherit = "account.invoice"

    bind_concept = fields.Selection(
        [('ALQ', 'Alquiler'),
         ('CUO', 'Cuota'),
         ('EXP','Expensas'),
         ('FAC','Factura'),
         ('PRE','Préstamo'),
         ('SEG','Seguro'),
         ('HON','Honorarios'),
         ('HAB','Haberes'),
         ('VAR','Varios')],
        'Concepto',
        default = 'VAR'
    )