{
    'name' : 'BIND PAYMENT',
    'description': 'Webhook BIND',
    'author': 'Matias',
    'depends': ['bind', 'bind_account', 'account_payment_group','account_document','account'],
    'application' : True,
    'data' : ["views/bind_account_journal.xml",
            "views/bind_account_invoice.xml"]
}
